<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    //EJERCICIO CONSULTAS
    
    //EJERCICIO 1 
    
//-- (1) listar las edades de los ciclistas (sin repetidos)
//select distinct edad from ciclistas;
    
    //Metodo DAO
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta1a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("edad")->distinct(),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
            
    }
    
    //EJERCICIO 2
    
//-- (2) listar las edades de los ciclistas de Artiach
//select distinct edad from ciclista where nomequipo='Artiach';
    
    //Metodo DAO
    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta2a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("edad")->distinct()->where('nomequipo="Artiach"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
            
    }
    
    //EJERCICIO 3
    
//-- (3) listar las edades de los ciclistas de Artiach o de Amore Vita
//select distinct edad from ciclista where nomequipo='Artiach' OR nomequipo='Amore Vita';  
    
    //Metodo DAO
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach" or nomequipo="Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach" or nomequipo="Amore Vita"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta3a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("edad")->distinct()->where('nomequipo="Artiach" OR nomequipo="Amore Vita"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
            
    }
    
    //EJERCICIO 4
    
//-- (4) listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30
//select dorsal from ciclista where edad < 25 OR edad > 30;
    
    //Metodo DAO
    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from ciclista where edad < 25 OR edad > 30')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad < 25 OR edad > 30',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad < 25 OR edad > 30",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta4a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("dorsal")->distinct()->where('edad < 25 OR edad > 30'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con ORM",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad < 25 OR edad > 30",
        ]);
            
    }
    
      //EJERCICIO 5
    
//-- (5) listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto
//select dorsal from ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32;
    
    //Metodo DAO
    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 28 AND 32')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 28 AND 32',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"select dorsal from ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta5a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("dorsal")->where('nomequipo="Banesto" AND edad BETWEEN 28 AND 32'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con ORM",
            "enunciado"=>"Llistar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"select dorsal from ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32",
        ]);
            
    }
    
    //EJERCICIO 6
    
//-- (6) Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8
//select nombre from ciclista where char_length(nombre)>8;

    //Metodo DAO
    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct nombre) from ciclista where char_length(nombre)>8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nombre from ciclista where char_length(nombre)>8',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"select nombre from ciclista where char_length(nombre)>8",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta6a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nombre")->where('char_length(nombre)>8'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con ORM",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"select nombre from ciclista where char_length(nombre)>8",
        ]);
            
    }
    
     //EJERCICIO 7
    
//-- (7) lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas
//select nombre, dorsal, UPPER(nombre) as 'nombre mayusculas' from ciclista;    
    
    //Metodo DAO
    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(nombre), dorsal, UPPER(nombre) as nombremayusculas from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nombre, dorsal, UPPER(nombre) as nombremayusculas from ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombremayusculas'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"select nombre, dorsal, UPPER(nombre) as 'nombremayusculas' from ciclista",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta7a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nombre,dorsal,upper(nombre)nombremayusculas")->distinct(),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombremayusculas'],
            "titulo"=>"Consulta 7 con ORM",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"select nombre, dorsal, UPPER(nombre) as 'nombremayusculas' from ciclista",
        ]);
            
    }
    
    //EJERCICIO 8
    
//-- (8) Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa
//select distinct dorsal from lleva WHERE código='MGE';
    
    //Metodo DAO
    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from lleva WHERE código="MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct dorsal from lleva WHERE código="MGE"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"select distinct dorsal from lleva WHERE codigo='MGE'",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta8a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Lleva::find()->SELECT("dorsal")->distinct()->where('código="MGE"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con ORM",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"select distinct dorsal from lleva WHERE codigo='MGE'",
        ]);
            
    }
    
    //EJERCICIO 9

//-- (9) Listar el nombre de los puertos cuya altura sea mayor de 1500 
//select nompuerto from puerto where altura>1500;    
    
    //Metodo DAO
    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto where altura>1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nompuerto from puerto where altura>1500',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"select nompuerto from puerto where altura>1500",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta9a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find()->SELECT("nompuerto")->distinct()->where('altura>1500'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con ORM",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"select nompuerto from puerto where altura>1500",
        ]);
            
    }
    
    //EJERCICIO 10

//-- (10) Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000
//select distinct dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000;    
    
    //Metodo DAO
    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from puerto where pendiente>8 OR altura between 1800 AND 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"select distinct dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta10a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find()->SELECT("dorsal")->distinct()->where('pendiente>8 OR altura BETWEEN 1800 AND 3000'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con ORM",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"select distinct dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
            
    }
    
    //EJERCICIO 11

//-- (10) Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000
//select distinct dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000;    
    
    //Metodo DAO
    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from puerto where pendiente>8 AND altura between 1800 AND 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"select distinct dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta11a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find()->SELECT("dorsal")->distinct()->where('pendiente>8 AND altura BETWEEN 1800 AND 3000'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con ORM",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"select distinct dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
            
    }
    
}
